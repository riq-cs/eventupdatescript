import logging
from datetime import datetime
import requests

def event_tags(events, auth, **changes):
    '''
    function: event_tags
    Submits changes to selected events. Will overwrite all tags, rather than adding to them.
    variables:
        events - List of event IDs
        api_token - String containing API Token
        api_key - String containing API Secret Key
        tags - List of Strings containing the individual tag changes.
        review_code: String containing the new review code.
    returns: None
    '''
    #get commands to post to API from eventIDs.txt
    logging.info("Starting process.")

    #build events from eventIDs.txt
    #build actual request
    json_dump = {}
    changes["ids"] = events
    logging.debug(changes)
    #posts changes to API
    response = requests.post(
        url="https://ws.riskiq.net/v1/event/update",
        auth=auth,
        headers={
            "Content-Type": "application/json",
        },
        json=changes
    )
    if response.status_code != 200:
        logging.critical('HTTP Request failed.')
        logging.critical('Response HTTP Status Code: {}'.format(response.status_code))
        logging.critical('Response HTTP Response Body: {}'.format(response.content))
    return response

if __name__ == '__main__':
    auth = ('token', 'key') # insert token & key here
    ids = open('events.txt', 'rb').read().splitlines()
    print event_tags(ids, auth, reviewCode = 'Dismissed')